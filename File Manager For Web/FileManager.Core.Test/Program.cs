﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileManager.Core.Abstract;

namespace FileManager.Core.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            IFileManager fileManager = new TeoFileManager(@"D:\tomcat\lib");
            Console.WriteLine(fileManager.Directories);
            Console.WriteLine(fileManager.Files);
            fileManager.MoveBack();
            Console.WriteLine(fileManager.Directories);
            Console.WriteLine(fileManager.Files);
            fileManager.MoveToRoot();
            Console.WriteLine(fileManager.Directories);
            Console.WriteLine(fileManager.Files);
            Console.WriteLine(fileManager.CountLessFiles(2048));
            Console.WriteLine(fileManager.CountBiggerFiles(2048));
            var d = fileManager.Drivers;
        }
    }
}
