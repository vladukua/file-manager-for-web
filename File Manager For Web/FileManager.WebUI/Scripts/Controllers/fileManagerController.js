﻿var fileManagerApp = angular.module('fileManagerApp', []);
fileManagerApp.controller("fileManagerController", function ($scope, fileManagerService) {
    fileManagerService.getContent().
    then(handleSuccess);

    $scope.openDirectory = function ($event) {
        var el = $event.currentTarget;
        var section = el.attributes["data-section"].value;
        if (section === "directory") {
            var directory = el.attributes["data-name"].value;
            fileManagerService.moveToDirectory(directory).
                then(handleSuccess);
        }
    }

    $scope.openDrive = function($event) {
        var el = $event.currentTarget;
        var section = el.attributes["data-section"].value;
        if (section === "drive") {
            var drive = el.attributes["data-name"].value;
            fileManagerService.moveToDrive(drive).
                then(handleSuccess);
        }
    }

    $scope.moveToBack = function($event) {
        var el = $event.currentTarget;
        var section = el.attributes["data-section"].value;
        if (section === "back") {
            fileManagerService.moveToBack().
                then(handleSuccess);
        }
    }

    function handleSuccess(content) {
        $scope.directories = content.directories;
        $scope.path = content.path;
        $scope.files = content.files;
        $scope.directoriesCount = content.directoriesCount;
        $scope.filesCount = content.filesCount;
        $scope.filesLessBorder = content.filesLessBorder;
        $scope.filesBetweenBorders = content.filesBetweenBorders;
        $scope.filesMoreBorder = content.filesMoreBorder;
        $scope.drivers = content.drivers;
    }
});

fileManagerApp.directive('repeatDirective', function () {
    return function(scope, element, attrs) {
        element.tooltip();
    };
})

fileManagerApp.service("fileManagerService", function ($http, $location) {
    return ({
        getContent: getContent,
        moveToDirectory: moveToDirectory,
        moveToBack: moveToBack,
        moveToDrive: moveToDrive
    });

    function getContent() {
        var url = $location.url();
        url += "/api/FileApi";
        var request = $http({
            method: "get",
            url: url
        });
        return (request.then(handleSuccess));
    }

    function moveToDirectory(directory) {
        var url = $location.url();
        url += "/api/FileApi";
        var request = $http({
            method: "get",
            url: url,
            params: {
                action: "moveToDirectory",
                directoryName: directory
            }
        });
        return (request.then(handleSuccess));
    }

    function moveToDrive(drive) {
        var url = $location.url();
        url += "/api/FileApi";
        var request = $http({
            method: "get",
            url: url,
            params: {
                action: "moveToDrive",
                directoryName: drive
            }
        });
        return (request.then(handleSuccess));
    }

    function moveToBack() {
        var url = $location.url();
        url += "/api/FileApi";
        var request = $http({
            method: "get",
            url: url,
            params: {
                action: "MoveToBack"
            }
        });
        return (request.then(handleSuccess));
    }

    function handleSuccess(response) {
        return (response.data);
    }
});