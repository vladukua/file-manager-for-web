﻿using System.Web.Optimization;

namespace FileManager.WebUI
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/styles").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css",
                      "~/Content/hover.css"));

            #region Scripts bundles

            #region jQuery Scripts

            bundles.Add(new ScriptBundle("~/bundles/jquery/scripts").Include(
                "~/Scripts/jquery-1.10.2.js"));

            #endregion

            #region Angular Scripts

            bundles.Add(new ScriptBundle("~/bundles/angular/scripts").Include(
                "~/Scripts/angular.js"));

            #endregion

            #region Libs Scripts

            bundles.Add(new ScriptBundle("~/bundles/libs/scripts").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/modernizr-2.6.2.js"));

            #endregion

            #region Common Scripts

            bundles.Add(new ScriptBundle("~/bundles/common/scripts").Include(
                "~/Scripts/Pages/file.js",
                "~/Scripts/Controllers/fileManagerController.js"));

            #endregion

            #endregion

            BundleTable.EnableOptimizations = false;
        }
    }
}