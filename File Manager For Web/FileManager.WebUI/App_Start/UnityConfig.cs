﻿using System.Web;
using System.Web.Http;
using FileManager.Core;
using FileManager.Core.Abstract;
using FileManager.WebUI.Code;
using Microsoft.Practices.Unity;

namespace FileManager.WebUI
{
    public static class UnityConfig
    {
        public static void RegisterComponents(HttpConfiguration config)
        {
            var container = new UnityContainer();
            var path = HttpContext.Current.Server.MapPath("");
            container.RegisterType<IFileManager, TeoFileManager>(new InjectionConstructor(path));
            var session = HttpContext.Current.Session;
            config.DependencyResolver = new UnityResolver(container);
        }
    }
}