﻿using System.Text;
using System.Web;
using System.Web.Http;
using FileManager.Core.Abstract;

namespace FileManager.WebUI.Controllers
{
    public class FileApiController : ApiController
    {
        #region Private Fields

        private readonly IFileManager _fileManager;

        #endregion

        #region Constructors

        public FileApiController(IFileManager fileManager)
        {
            _fileManager = fileManager;
            var path = HttpContext.Current.Session["CurrentPath"];
            if (path != null)
            {
                _fileManager.MoveTo(path as string);
            }
        }

        #endregion

        #region HttpActions

        [HttpGet]
        public object Get()
        {
            var directories = _fileManager.DirectoryNames;
            var files = _fileManager.FileNames;
            var path = _fileManager.Path;
            HttpContext.Current.Session["CurrentPath"] = path;
            return Json(new
            {
                path = path,
                directories = directories,
                files = files,
                directoriesCount = _fileManager.DirectoriesCount,
                filesCount = _fileManager.FilesCount,
                filesLessBorder = _fileManager.CountLessFiles(10485760),
                filesMoreBorder = _fileManager.CountBiggerFiles(104857600),
                filesBetweenBorders = _fileManager.CountBetweenFiles(10485760, 52428800),
                drivers = _fileManager.DriversNames
            });
        }

        [HttpGet]
        public object Get(string action, string directoryName)
        {
            var sb = new StringBuilder();
            if (action == "moveToDirectory")
            {
                sb.Append(_fileManager.Path).Append("\\").Append(directoryName);
            }
            else if (action == "moveToDrive")
            {
                sb.Append(directoryName);
            }
            _fileManager.MoveTo(sb.ToString());
            var directories = _fileManager.DirectoryNames;
            var files = _fileManager.FileNames;
            var path = _fileManager.Path;
            HttpContext.Current.Session["CurrentPath"] = path;
            return Json(new
            {
                path = path,
                directories = directories,
                files = files,
                directoriesCount = _fileManager.DirectoriesCount,
                filesCount = _fileManager.FilesCount,
                filesLessBorder = _fileManager.CountLessFiles(10485760),
                filesMoreBorder = _fileManager.CountBiggerFiles(104857600),
                filesBetweenBorders = _fileManager.CountBetweenFiles(10485760, 52428800),
                drivers = _fileManager.DriversNames
            });
        }

        [HttpGet]
        public object Get(string action)
        {
            if (action == "MoveToBack")
            {
                _fileManager.MoveBack();
            }
            var directories = _fileManager.DirectoryNames;
            var files = _fileManager.FileNames;
            var path = _fileManager.Path;
            HttpContext.Current.Session["CurrentPath"] = path;
            return Json(new
            {
                path = path,
                directories = directories,
                files = files,
                directoriesCount = _fileManager.DirectoriesCount,
                filesCount = _fileManager.FilesCount,
                filesLessBorder = _fileManager.CountLessFiles(10485760),
                filesMoreBorder = _fileManager.CountBiggerFiles(104857600),
                filesBetweenBorders = _fileManager.CountBetweenFiles(10485760, 52428800),
                drivers = _fileManager.DriversNames
            });
        }

        #endregion
    }
}
