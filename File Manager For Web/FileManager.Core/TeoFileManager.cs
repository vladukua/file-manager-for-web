﻿using System;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using FileManager.Core.Abstract;

namespace FileManager.Core
{
    public class TeoFileManager : IFileManager
    {
        #region Private Fields

        private DirectoryInfo _directoryInfo;
        private readonly DriveInfo[] _driveInfo;
        private readonly string _rootPath;

        #endregion

        #region Constructors

        public TeoFileManager(string path)
        {
            _directoryInfo = new DirectoryInfo(path);
            _rootPath = path;
            _driveInfo = DriveInfo.GetDrives();
        }

        #endregion

        #region IFileManager

        public FileInfo[] Files
        {
            get { return _directoryInfo.GetFiles(); }
        }

        public DirectoryInfo[] Directories
        {
            get
            {
                return _directoryInfo.GetDirectories().Where(d =>
                    {
                        try
                        {
                            d.GetDirectories();
                            return true;
                        }
                        catch (UnauthorizedAccessException)
                        {
                            return false;
                        }
                    }).ToArray();
            }
        }

        public DriveInfo[] Drivers
        {
            get
            {
                return
                    _driveInfo.Where(d => d.IsReady).ToArray();
            }
        }

        public string[] FileNames
        {
            get { return Files.Select(f => f.Name).ToArray(); }
        }

        public string[] DirectoryNames
        {
            get { return Directories.Select(d => d.Name).ToArray(); }
        }

        public string[] DriversNames
        {
            get { return Drivers.Select(d => d.Name).ToArray(); }
        }

        public string Path
        {
            get { return _directoryInfo.FullName; }
        }

        public bool MoveTo(string destination)
        {
            _directoryInfo = new DirectoryInfo(destination);
            return true;
        }

        public int CountLessFiles(int bytes)
        {
            var files = _directoryInfo.GetFiles();
            return files.Count(f => f.Length < bytes);
        }

        public int CountBiggerFiles(int bytes)
        {
            var files = _directoryInfo.GetFiles();
            return files.Count(f => f.Length > bytes);
        }

        public int CountBetweenFiles(int loverBoundBytes, int upperBoundBytes)
        {
            var files = _directoryInfo.GetFiles();
            return files.Count(f => f.Length > loverBoundBytes && f.Length < upperBoundBytes);
        }

        public bool MoveBack()
        {
            _directoryInfo = _directoryInfo.Parent ?? _directoryInfo;
            return _directoryInfo.Parent != null;
        }

        public void MoveToRoot()
        {
            _directoryInfo = new DirectoryInfo(_rootPath);
        }

        public int DirectoriesCount
        {
            get { return Directories.Length; }
        }

        public int FilesCount
        {
            get { return Files.Length; }
        }

        #endregion
    }
}
