﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Core.Abstract
{
    public interface IFileManager
    {
        FileInfo[] Files { get; }
        DirectoryInfo[] Directories { get; }
        DriveInfo[] Drivers { get; }
        string[] FileNames { get; }
        string[] DirectoryNames { get; }
        string[] DriversNames { get; }
        string Path { get; }
        bool MoveTo(string destination);
        int CountLessFiles(int bytes);
        int CountBiggerFiles(int bytes);
        int CountBetweenFiles(int loverBoundBytes, int upperBoundBytes);
        bool MoveBack();
        void MoveToRoot();
        int DirectoriesCount { get; }
        int FilesCount { get; }
    }
}
